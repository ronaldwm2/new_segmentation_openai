import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('segmentation')
export class Segmentation {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  url: string | null;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  title: string | null;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  category: string | null;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  segmentation: string | null;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  hostname: string | null;
}