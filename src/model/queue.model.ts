import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity('queue')
export class Queue {
  @PrimaryColumn({ type: 'varchar', length: 255, collation: 'utf8mb4_0900_ai_ci' })
  url: string;
  
  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  title: string | null;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  category: string | null;

  @Column({ type: 'varchar', length: 255, nullable: true, collation: 'utf8mb4_0900_ai_ci' })
  hostname: string | null;

  @Column({ type: 'varchar', length: 255, default: 'false', collation: 'utf8mb4_0900_ai_ci' })
  is_process: string;

  @Column({ type: 'int' })
  articleViews: number;

  @Column({ type: 'int' })
  ready: number;
}