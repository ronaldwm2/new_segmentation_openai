import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { mysqlDataSource } from 'src/datasource';
import { Queue } from 'src/model/queue.model';
@Injectable()
export class QueueService {

    async fetchTopArticle() {
        return await new Promise((resolve, reject) => {
            axios.get("https://ga4.props.id/ga4-alldata")
            .then((result) => {
                resolve(this.filterOtherData(result.data));
            }).catch((err) => {
                reject(err);
            })
        });
    }

    async filterOtherData(array) {
        let statsArr1 =  array.filter((e) => {return e.pageUrl != "(other)"})
        let statsArr2 = statsArr1.filter((e) => {return e.pageTitle != "(other)"});

        return statsArr2;
    }

    async fetchTopInformation() {
        let queue = await mysqlDataSource.getRepository(Queue)
            .createQueryBuilder("queue")
            .where("queue.is_process = 'false' AND queue.ready = 0")
            .orderBy('articleViews', "DESC")
            .limit(5)
            .getMany();

        return queue;
        
    }

    async startFetchingInformation(fetchData) {
        for (let row1 of fetchData) {
            let arr = [];
            const input = row1.url;
            const regex = /\/(.+)/;
            const match = input.match(regex);
            
            if (match) {
                const path = match[1];
                console.log(path);
                let inf : any = await new Promise((resolve, reject) => {
                axios.get(`https://ga4.props.id/get-data-pagepath/gender?pagepath=${path}&startdate=2020-01-01&enddate=2024-12-31`)
                    .then((result) => {
                        resolve(result.data);
                    }).catch((err) => {
                        reject(err);
                    })
                });
                let filtergenderUnknown = inf.filter((e) => {return e.gender != "unknown"});
                let arrFilter = await this.filterDuplicate(filtergenderUnknown, "gender");
                let sorted = arrFilter.sort((b,a) => {return a.activeUser - b.activeUser});

                let inf2 : any = await new Promise((resolve, reject) => {
                axios.get(`https://ga4.props.id/get-data-pagepath/ages?pagepath=${path}&startdate=2020-01-01&enddate=2024-12-31`)
                    .then((result) => {
                        resolve(result.data);
                    }).catch((err) => {
                        reject(err);
                    })
                });
                let filterageUnknown = inf2.filter((e) => {return e.age != "unknown"});
                let arrFilterAge = await this.filterDuplicate(filterageUnknown, "age");
                let sortedAge = arrFilterAge.sort((b,a) => {return a.age - b.age});
                arr.push(sorted[0].gender);
                arr.push(sortedAge[0].age);
            } else {
                continue;
            }

            row1.category = arr;
        }

        return fetchData;

    }

    async updateToQueue(fetchData) {
        for (let row of fetchData) {
            row.ready = 1;
            row.category = JSON.stringify(row.category);
            await mysqlDataSource.getRepository(Queue)
                .createQueryBuilder("queue")
                .update()
                .set(row)
                .where("id = :id", {id: row.id})
                .execute();
            }

    }

    async filterDuplicate(arr, target) {
        let finArr = [];
        for (let row of arr) {
            let match = finArr.find((e) => {return e[target] == row[target]});
            if (!match) {
                finArr.push(row);
            }
        }
        return finArr;
    }

    async startInsertingToQueue(array) {
        let dataToInsert = [];
        for (let row of array) {
            const regex = /^(?:https?:\/\/)?(?:www\.)?([^\/]+)/;
            const match = row.pageUrl.match(regex);
            let hostname = "";
            if (match) {
                hostname = match[1];
                // console.log(hostname);
            } else {
                console.log("No match found");
                continue;
            }
            let data = {
                url: row.pageUrl,
                title: row.pageTitle,
                articleViews: row.activeUser,
                hostname: hostname,
                is_process: "false"
            }
            dataToInsert.push(data);
        }

        let doProcess = await mysqlDataSource.getRepository(Queue).createQueryBuilder()
            .insert()
            .into(Queue)
            .values(dataToInsert)
            .orIgnore()
            .execute();

        return doProcess;
    }
}
