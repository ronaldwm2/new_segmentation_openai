import { Body, Controller, Get, Req, Res } from '@nestjs/common';
import { QueueService } from './queue.service';

@Controller("queue")
export class QueueController {
  constructor(private readonly queueService: QueueService) {}

  @Get("getTopArticle")
  async getTopArticle(@Req() req, @Res() res, @Body() body) {
    try{
        let fetchData = await this.queueService.fetchTopArticle();
        console.log(fetchData);
        res.send(fetchData);
    } catch (e) {
        console.log(e);
        res.status(500);
        res.send({
            status: "error",
            reason: e.message
        })
    }
  }

  @Get("insertTopArticleToQueue")
  async insertTopArticleToQueue(@Req() req, @Res() res, @Body() body) {
    try{
        let fetchData = await this.queueService.fetchTopArticle();
        let startInserting = await this.queueService.startInsertingToQueue(fetchData);
        res.send(startInserting);
    } catch (e) {
        console.log(e);
        res.status(500);
        res.send({
            status: "error",
            reason: e.message
        })
    }
  }

  @Get("getPageInformation")
  async getPageInformation(@Req() req, @Res() res, @Body() body) {
    try{
        let fetchData = await this.queueService.fetchTopInformation();
        let startFetchingInformation = await this.queueService.startFetchingInformation(fetchData);
        let updateToQueue = await this.queueService.updateToQueue(fetchData);
        res.send(startFetchingInformation);
    } catch (e) {
        console.log(e);
        res.status(500);
        res.send({
            status: "error",
            reason: e.message
        })
    }
  }
}
