import { Injectable } from '@nestjs/common';
import { mysqlDataSource } from './datasource';
import { Segmentation } from './model/segmentation.model';

@Injectable()
export class AppService {
  async getHello() {
    return await mysqlDataSource.getRepository(Segmentation).createQueryBuilder("test")
      .limit(50)
      .getMany();

    
  }
}
