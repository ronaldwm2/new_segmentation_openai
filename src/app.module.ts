import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { mysqlDataSource } from './datasource';
import { QueueController } from './queue/queue.controller';
import { QueueService } from './queue/queue.service';

@Module({
  imports: [],
  controllers: [AppController, QueueController],
  providers: [AppService, QueueService],
})
export class AppModule {
  constructor() {
    mysqlDataSource.initialize().then(() => {
      console.log("datasource initialized");
    })
  }
}
