import { DataSource } from "typeorm"
import { Segmentation } from "./model/segmentation.model"
import { Queue } from "./model/queue.model"

export const mysqlDataSource = new DataSource({
    type: "mysql",
    host: "172.104.36.90",
    port: 3306,
    username: "ronaldwm",
    password: "password",
    database: "segmentation_openai",
    entities: [
        Segmentation,
        Queue
    ],
})

// const PostgresDataSource = new DataSource({
//     type: "postgres",
//     host: "localhost",
//     port: 5432,
//     username: "test",
//     password: "test",
//     database: "test",
//     entities: [
//         // ....
//     ],
// })